package com.edataconsulting.demo.service;

import com.edataconsulting.demo.entity.Permissions;
import com.edataconsulting.demo.repositories.PermissionsRepository;
import com.edataconsulting.demo.services.PermissionsServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class PermissionServiceTest {
    private PermissionsServices permissionsServices;
    @MockBean
    private PermissionsRepository permissionsRepository;
    @BeforeEach
    void setUp(){
        permissionsServices = new PermissionsServices();
    }
    @Test
    void whenCallFindAll_shouldCallMessageRepositoryFindAllAndReturnResult() {
        List<Permissions> permissions = new ArrayList<>();
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");
        permissions.add(permission);

        when(permissionsRepository.findAll()).thenReturn(permissions);

        List<Permissions> result = permissionsServices.findAll();

        verify(permissionsRepository).findAll();
        assertThat(result).isEqualTo(permissions);
    }
    @Test
    void whenCallFindById_shouldCallPersonRepositoryFindByIdAndReturnResult() {
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");

        when(permissionsRepository.findById(permission.getId())).thenReturn(Optional.of(permission));

        Permissions result = permissionsServices.findById(permission.getId());

        verify(permissionsRepository).findById(permission.getId());
        assertThat(result).isEqualTo(permission);
    }
    @Test
    void whenCallAdd_shouldCallPersonRepositoryAddAndReturnResult() {
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");

        when(permissionsRepository.save(permission)).thenReturn(permission);

        Permissions result = permissionsServices.add(permission);

        verify(permissionsRepository).save(permission);
        assertThat(result).isEqualTo(permission);
    }
    @Test
    void whenCallUpdate_shouldCallPersonRepositoryUpdateAndReturnResult() {
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");

        when(permissionsRepository.save(permission)).thenReturn(permission);

        Permissions result = permissionsServices.add(permission);

        verify(permissionsRepository).save(permission);
        assertThat(result).isEqualTo(permission);
    }
    @Test
    void whenCallDelete_shouldCallPersonRepositoryDelete() {
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");

        verify(permissionsRepository).deleteById(permission.getId());
    }
}


