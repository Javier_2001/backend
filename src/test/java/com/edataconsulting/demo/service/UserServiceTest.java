package com.edataconsulting.demo.service;

import com.edataconsulting.demo.entity.Users;
import com.edataconsulting.demo.repositories.UserRepository;
import com.edataconsulting.demo.services.UsersServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceTest {
    private UsersServices usersServices;
    @MockBean
    private UserRepository userRepository;
    @BeforeEach
    void setUp(){
        usersServices = new UsersServices();
    }
    @Test
    void whenCallFindAll_shouldCallUsersRepositoryFindAllAndReturnResult() {
        List<Users> users = new ArrayList<>();
        Users user = new Users();
        user.setId(1);
        user.setName("Javier");
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());
        users.add(user);

        when(userRepository.findAll()).thenReturn(users);

        List<Users> result = usersServices.findAll();

        verify(userRepository).findAll();
        assertThat(result).isEqualTo(users);
    }
    @Test
    void whenCallFindById_shouldCallPersonRepositoryFindByIdAndReturnResult() {
        Users user = new Users();
        user.setId(1);
        user.setName("Javier");
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());

        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        Users result = usersServices.findById(user.getId());

        verify(userRepository).findById(user.getId());
        assertThat(result).isEqualTo(user);
    }
    @Test
    void whenCallAdd_shouldCallPersonRepositoryAddAndReturnResult() {
        Users user = new Users();
        user.setId(1);
        user.setName("Javier");
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());

        when(userRepository.save(user)).thenReturn(user);

        Users result = usersServices.add(user);

        verify(userRepository).save(user);
        assertThat(result).isEqualTo(user);
    }
    @Test
    void whenCallUpdate_shouldCallPersonRepositoryUpdateAndReturnResult() {
        Users user = new Users();
        user.setId(1);
        user.setName("Javier");
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());

        when(userRepository.save(user)).thenReturn(user);

        Users result = usersServices.add(user);

        verify(userRepository).save(user);
        assertThat(result).isEqualTo(user);
    }
    @Test
    void whenCallDelete_shouldCallPersonRepositoryDelete() {
        Users user = new Users();
        user.setId(1);
        user.setName("Javier");
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());

        verify(userRepository).deleteById(user.getId());
    }
}
