package com.edataconsulting.demo.service;

import com.edataconsulting.demo.entity.Messages;
import com.edataconsulting.demo.entity.Users;
import com.edataconsulting.demo.repositories.MessagesRepository;
import com.edataconsulting.demo.repositories.UserRepository;
import com.edataconsulting.demo.services.MessagesServices;
import com.edataconsulting.demo.services.UsersServices;
import org.aspectj.bridge.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class MessageServiceTest {
    private MessagesServices messagesService;
    @MockBean
    private MessagesRepository messagesRepository;
    @BeforeEach
    void setUp(){
        messagesService = new MessagesServices();
    }
    @Test
    void whenCallFindAll_shouldCallMessageRepositoryFindAllAndReturnResult() {
        List<Messages> messages = new ArrayList<>();
        Messages message = new Messages();
        message.setId(1);
        message.setText("ahwdwfjnffbiadbdvosjn");
        message.setDate(LocalDate.now());
        messages.add(message);

        when(messagesRepository.findAll()).thenReturn(messages);

        List<Messages> result = messagesService.findAll();

        verify(messagesRepository).findAll();
        assertThat(result).isEqualTo(messages);
    }
    @Test
    void whenCallFindById_shouldCallPersonRepositoryFindByIdAndReturnResult() {
        Messages message = new Messages();
        message.setId(1);
        message.setText("ahwdwfjnffbiadbdvosjn");
        message.setDate(LocalDate.now());

        when(messagesRepository.findById(message.getId())).thenReturn(Optional.of(message));

        Messages result = messagesService.findById(message.getId());

        verify(messagesRepository).findById(message.getId());
        assertThat(result).isEqualTo(message);
    }
    @Test
    void whenCallAdd_shouldCallPersonRepositoryAddAndReturnResult() {
        Messages message = new Messages();
        message.setId(1);
        message.setText("ahwdwfjnffbiadbdvosjn");
        message.setDate(LocalDate.now());

        when(messagesRepository.save(message)).thenReturn(message);

        Messages result = messagesService.add(message);

        verify(messagesRepository).save(message);
        assertThat(result).isEqualTo(message);
    }
    @Test
    void whenCallUpdate_shouldCallPersonRepositoryUpdateAndReturnResult() {
        Messages message = new Messages();
        message.setId(1);
        message.setText("ahwdwfjnffbiadbdvosjn");
        message.setDate(LocalDate.now());

        when(messagesRepository.save(message)).thenReturn(message);

        Messages result = messagesService.add(message);

        verify(messagesRepository).save(message);
        assertThat(result).isEqualTo(message);
    }
    @Test
    void whenCallDelete_shouldCallPersonRepositoryDelete() {
        Messages message = new Messages();
        message.setId(1);
        message.setText("ahwdwfjnffbiadbdvosjn");
        message.setDate(LocalDate.now());

        verify(messagesRepository).deleteById(message.getId());
    }
}
