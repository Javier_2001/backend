package com.edataconsulting.demo.controller;

import com.edataconsulting.demo.controllers.PermissionsController;
import com.edataconsulting.demo.controllers.UsersController;
import com.edataconsulting.demo.entity.Permissions;
import com.edataconsulting.demo.entity.Users;
import com.edataconsulting.demo.services.PermissionsServices;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class PermissionControllerTest {
    private static final String PERMISSION_ENDPOINT = "/permissions";
    @MockBean
    private PermissionsServices permissionsServices;
    @Autowired
    private MockMvc mockMvc;
    @Test
    public void whenRequestFindAll_thenReturnAll() throws Exception {
        List<Permissions> permissions = new ArrayList<>();
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");
        permissions.add(permission);
        when(permissionsServices.findAll()).thenReturn(permissions);

        mockMvc.perform(MockMvcRequestBuilders.get(PERMISSION_ENDPOINT))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermissionsController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findAll"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name").value("Editar"));
    }
    @Test
    public void whenRequestFindById_thenReturnAll() throws Exception {
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");

        when(permissionsServices.findById(any(Long.class))).thenReturn(permission);

        mockMvc.perform(MockMvcRequestBuilders.get(PERMISSION_ENDPOINT + "/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findById"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Editar"));
    }
    @Test
    public void whenRequestAdd_thenReturnMessage() throws Exception {
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");

        String  mock = "{\"name\":\"Editar\"}";

        when(permissionsServices.add(permission)).thenReturn(permission);

        mockMvc.perform(MockMvcRequestBuilders.post(PERMISSION_ENDPOINT)
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermissionsController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Editar"));
    }
    @Test
    public void whenRequestUpdate_thenReturnAll() throws Exception {
        Permissions permission = new Permissions();
        permission.setId(1);
        permission.setName("Editar");

        String  mock = "{\"name\":\"Editar\"}";

        when(permissionsServices.update(permission.getId(), permission)).thenReturn(permission);

        mockMvc.perform(MockMvcRequestBuilders.put(PERMISSION_ENDPOINT)
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermissionsController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("update"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name").value("Editar"));
    }
    @Test
    public void whenRequestDelete_thenCheckMethodIsCall() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete(PERMISSION_ENDPOINT+"/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermissionsController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("delete"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
