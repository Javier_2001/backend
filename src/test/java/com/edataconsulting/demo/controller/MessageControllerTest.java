package com.edataconsulting.demo.controller;

import com.edataconsulting.demo.controllers.MessagesController;
import com.edataconsulting.demo.controllers.UsersController;
import com.edataconsulting.demo.entity.Messages;
import com.edataconsulting.demo.entity.Permissions;
import com.edataconsulting.demo.entity.Users;
import com.edataconsulting.demo.services.MessagesServices;
import com.edataconsulting.demo.services.UsersServices;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {
    private static final String MESSAGES_ENDPOINT = "/messages";
    @MockBean
    private MessagesServices messagesServices;
    @Autowired
    private MockMvc mockMvc;
    @Test
    public void whenRequestFindAll_thenReturnAll() throws Exception {
        List<Messages> messages = new ArrayList<>();
        Messages message = new Messages()   ;
        message.setText("ahwdwfjnffbiadbdvosjn");
        message.setId(1);
        messages.add(message);
        when(messagesServices.findAll()).thenReturn(messages);

        mockMvc.perform(MockMvcRequestBuilders.get(MESSAGES_ENDPOINT))
                .andExpect(MockMvcResultMatchers.handler().handlerType(MessagesController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findAll"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].text").value("ahwdwfjnffbiadbdvosjn"));
    }
    @Test
    public void whenRequestFindById_thenReturnAll() throws Exception {
        Messages message = new Messages();
        message.setId(1);
        message.setText("ahwdwfjnffbiadbdvosjn");

        when(messagesServices.findById(any(Long.class))).thenReturn(message);

        mockMvc.perform(MockMvcRequestBuilders.get(MESSAGES_ENDPOINT + "/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findById"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("ahwdwfjnffbiadbdvosjn"));
    }
    @Test
    public void whenRequestAdd_thenReturnMessage() throws Exception {
        Messages message = new Messages();
        message.setText("ahwdwfjnffbiadbdvosjn");
        message.setId(1);

        String  mock = "{\"text\":\"ahwdwfjnffbiadbdvosjn\"}";

        when(messagesServices.add(message)).thenReturn(message);

        mockMvc.perform(MockMvcRequestBuilders.post(MESSAGES_ENDPOINT)
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(MessagesController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("ahwdwfjnffbiadbdvosjn"));
    }
    @Test
    public void whenRequestUpdate_thenReturnAll() throws Exception {
        Messages message = new Messages();
        message.setId(1);
        message.setText("ahwdwfjnffbiadbdvosjn");

        String  mock = "{\"text\":\"ahwdwfjnffbiadbdvosjn\"}";

        when(messagesServices.update(message.getId(), message)).thenReturn(message);

        mockMvc.perform(MockMvcRequestBuilders.put(MESSAGES_ENDPOINT)
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("update"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("ahwdwfjnffbiadbdvosjn"));
    }
    @Test
    public void whenRequestDelete_thenCheckMethodIsCall() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete(MESSAGES_ENDPOINT+"/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("delete"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
