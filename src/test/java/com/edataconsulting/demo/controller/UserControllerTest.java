package com.edataconsulting.demo.controller;

import com.edataconsulting.demo.controllers.UsersController;
import com.edataconsulting.demo.entity.Users;
import com.edataconsulting.demo.services.UsersServices;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    private static final String USERS_ENDPOINT = "/users";
    @MockBean
    private UsersServices usersServices;
    @Autowired
    private MockMvc mockMvc;
    @Test
    public void whenRequestFindAll_thenReturnAll() throws Exception {
        List<Users> users = new ArrayList<>();
        Users user = new Users();
        user.setName("Javier");
        user.setId(1);
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());
        users.add(user);
        when(usersServices.findAll()).thenReturn(users);

        mockMvc.perform(MockMvcRequestBuilders.get(USERS_ENDPOINT))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findAll"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name").value("Javier"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].email").value("jmedinae8@gmail.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].state").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].date").value(LocalDate.now()));


    }
    @Test
    public void whenRequestFindById_thenReturnAll() throws Exception {
        Users user = new Users();
        user.setName("Javier");
        user.setId(1);
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());

        when(usersServices.findById(any(Long.class))).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.get(USERS_ENDPOINT + "/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findById"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Javier"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("jmedinae8@gmail.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.state").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("28-02-2009"));

    }
    @Test
    public void whenRequestAdd_thenReturnAll() throws Exception {
        Users user = new Users();
        user.setName("Javier");
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());

        String  mock = "{\"name\":\"Javier\",\"email\":\"jmedinae8@gmail.com\",\"state\":\"true\"}";

        when(usersServices.add(any(Users.class))).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.post(USERS_ENDPOINT)
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Javier"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("jmedinae8@gmail.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.state").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value(LocalDate.now()));


    }
    @Test
    public void whenRequestUpdate_thenReturnAll() throws Exception {
        Users user = new Users();
        user.setName("Javier");
        user.setEmail("jmedinae8@gmail.com");
        user.setState(true);
        user.setDate(LocalDate.now());

        String mock = "{\"name\":\"Javier\",\"email\":\"jmedinae8@gmail.com\",\"state\":\"true\"}";

        when(usersServices.update(user.getId(), user)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.put(USERS_ENDPOINT)
            .content(mock).contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
            .andExpect(MockMvcResultMatchers.handler().methodName("update"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Javier"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("jmedinae8@gmail.com"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.state").value(true))
            .andExpect(MockMvcResultMatchers.jsonPath("$.date").value(LocalDate.now()));
    }
    @Test
    public void whenRequestDelete_thenCheckMethodIsCall() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete(USERS_ENDPOINT+"/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("delete"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
