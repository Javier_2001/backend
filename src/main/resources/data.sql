insert into users (id, name, email, state) values (1, 'Jose', 'jose.borges@edataconsulting.de', true);
insert into users (id, name, email, state) values (2, 'David', 'david.koschel2002@gmail.com ', true);
insert into users (id, name, email, state) values (3, 'Pablo', 'pablonico71@gmail.com ', true);
insert into users (id, name, email, state) values (4, 'Javier', 'jmedinae8@gmail.com', true);
insert into messages (text, users, date) values ('message', 1, current_date());
insert into messages (text, users, date) values ('word', 1, current_date());
insert into messages (text, users, date) values ('text' , 2, current_date());
insert into messages (text, users, date) values ('example', 2, current_date());
insert into permissions (id, name) values (1, 'add_message');
insert into permissions (id, name) values (2, 'delete_message');
insert into permissions (id, name) values (3, 'add_user');
insert into permissions (id, name) values (4, 'delete_user');


