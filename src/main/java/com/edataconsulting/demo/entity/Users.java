package com.edataconsulting.demo.entity;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private LocalDate date;
    private String email;
    private boolean state = true;
    @ManyToMany
    @JoinTable(name="User_Perm",
        joinColumns = @JoinColumn(name = "Usersid"),
        inverseJoinColumns = @JoinColumn(name = "Permissionsid"))
    List<Permissions> permissions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }


}

