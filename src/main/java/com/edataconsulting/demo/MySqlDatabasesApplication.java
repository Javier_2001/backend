package com.edataconsulting.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySqlDatabasesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySqlDatabasesApplication.class, args);
	}

}
