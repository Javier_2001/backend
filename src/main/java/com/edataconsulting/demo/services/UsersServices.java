package com.edataconsulting.demo.services;

import com.edataconsulting.demo.entity.Users;
import com.edataconsulting.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UsersServices {
    @Autowired
    private UserRepository userRepository;

    public List<Users> findAll(){
        return userRepository.findAll();
    }
    public Users findById(long id){
        return userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }
    public Users add(Users users){
        return userRepository.save(users);
    }
    public Users update(long id, Users users){
        Users userDB = userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        userDB.setName(users.getName());
        userDB.setEmail(users.getEmail());
        return userRepository.save(userDB);
    }
    public void delete(long id){
        userRepository.deleteById(id);
    }
}
