package com.edataconsulting.demo.services;

import com.edataconsulting.demo.entity.Permissions;
import com.edataconsulting.demo.repositories.PermissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class PermissionsServices {
    @Autowired
    private PermissionsRepository permissionsRepository;

    public List<Permissions> findAll() {
        return permissionsRepository.findAll();
    }
    public Permissions findById(long id) {
        return permissionsRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }
    public Permissions add(Permissions permissions) {
        return permissionsRepository.save(permissions);
    }
    public Permissions update(long id, Permissions permissions){
        Permissions permissions1 = permissionsRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        permissions1.setName(permissions.getName());
        return permissionsRepository.save(permissions1);
    }
    public void delete(long id){
        permissionsRepository.deleteById(id);
    }
}