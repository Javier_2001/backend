package com.edataconsulting.demo.services;

import com.edataconsulting.demo.entity.Messages;
import com.edataconsulting.demo.repositories.MessagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class MessagesServices {
    @Autowired
    MessagesRepository messagesRepository;

    public List<Messages> findAll(){
        return messagesRepository.findAll();
    }
    public Messages findById(long id){
        return messagesRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }
    public Messages add(Messages messages){
        return messagesRepository.save(messages);
    }
    public Messages update(long id, Messages messages){
        Messages messages1 = messagesRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        messages1.setDate(messages.getDate());
        messages1.setText(messages.getText());
        return messagesRepository.save(messages1);
    }
    public void delete(long id){
        messagesRepository.deleteById(id);
    }
}
