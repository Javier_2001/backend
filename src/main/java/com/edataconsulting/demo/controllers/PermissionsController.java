package com.edataconsulting.demo.controllers;

import com.edataconsulting.demo.entity.Permissions;
import com.edataconsulting.demo.repositories.PermissionsRepository;
import com.edataconsulting.demo.services.PermissionsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("permissions")
@CrossOrigin
public class PermissionsController {
    @Autowired
    PermissionsServices permissionsServices;
    @GetMapping("")
    public List<Permissions> findAll(){
        return permissionsServices.findAll();
    }
    @GetMapping("{id}")
    public Permissions getPermission(@PathVariable long id){
        return permissionsServices.findById(id);
    }
    @PostMapping("")
    public Permissions add(@RequestBody Permissions permissions){
        return permissionsServices.add(permissions);
    }
    @PutMapping("{id}")
    public Permissions update(@PathVariable long id, @RequestBody Permissions permissions){
        return permissionsServices.update(id, permissions);
    }
    @DeleteMapping("{id}")
    public void delete(@PathVariable long id){
        permissionsServices.delete(id);
    }
}
