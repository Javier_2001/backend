package com.edataconsulting.demo.controllers;

import com.edataconsulting.demo.entity.Users;
import com.edataconsulting.demo.services.UsersServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
@CrossOrigin
public class UsersController {
    @Autowired
    private UsersServices usersServices;
    @GetMapping("")
    public List<Users> findAll() {
        return usersServices.findAll();
    }
    @GetMapping("{id}")
    public Users findById (@PathVariable long id){
        return usersServices.findById(id);
    }
    @PostMapping("")
    public Users add (@RequestBody Users users){
        return usersServices.add(users);
    }
    @PutMapping("{id}")
    public Users update(@PathVariable long id,@RequestBody Users users){
        return usersServices.update(id, users);
    }
    @DeleteMapping("{id}")
    public void delete(@PathVariable long id){
        usersServices.delete(id);
    }
}
