package com.edataconsulting.demo.controllers;

import com.edataconsulting.demo.entity.Messages;
import com.edataconsulting.demo.services.MessagesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("messages")
@CrossOrigin
public class MessagesController {
    @Autowired
    MessagesServices messagesServices;
    @GetMapping("")
    public List<Messages> findAll(){
        return messagesServices.findAll();
    }
    @GetMapping("{id}")
    public Messages findById(@PathVariable long id){
        return messagesServices.findById(id);
    }
    @PostMapping("")
    public Messages add(@RequestBody Messages messages){
        return messagesServices.add(messages);
    }
    @PutMapping("{id}")
    public Messages update(@PathVariable long id, @RequestBody Messages messages){
        return messagesServices.update(id, messages);
    }
    @DeleteMapping("{id}")
    public void delete(@PathVariable long id){
        messagesServices.delete(id);
    }
}
